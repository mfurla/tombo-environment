FROM ubuntu:18.04

ENV GUPPY_PACKAGE="ont-guppy_3.6.1_linux64.tar.gz"
# I downloaded this file from the official mirror: https://europe.oxfordnanoportal.com/software/analysis/ont-guppy_4.0.15_linux64.tar.gz
ENV XDG_RUNTIME_DIR=""
ENV PYTHONNOUSERSITE="not_empty_to_avoid_loading_packages_from_user_home"

MAINTAINER Mattia Furlan <mattia.furlan@iit.it>

RUN export DEBIAN_FRONTEND=noninteractive \
	&& apt-get update && \
	apt-get -y install \
		autoconf \
		python2.7 \
		python-pip \
		git \
		wget \
		unzip \
		lsb-release \
		apt-transport-https \
		vim \
		libidn11 \
		libz-dev \
		parallel

RUN pip install \
		numpy==1.16.6 \
		pandas==0.24.2 \
		patsy==0.5.1 \
		python-dateutil==2.8.1 \
		pytz==2020.1 \
		scipy==1.2.3 \
		statsmodels==0.10.0 \
		tqdm==4.32.1

RUN git clone https://github.com/nanoporetech/tombo.git \
	&& cd tombo \
	&& pip install -e .
